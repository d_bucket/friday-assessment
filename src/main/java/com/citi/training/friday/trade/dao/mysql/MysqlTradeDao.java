package com.citi.training.friday.trade.dao.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.citi.training.friday.trade.dao.TradeDao;
import com.citi.training.friday.trade.exception.TradeNotFoundException;
import com.citi.training.friday.trade.exception.TradeNotSqlCompatible;
import com.citi.training.friday.trade.model.Trade;

@Component
public class MysqlTradeDao implements TradeDao {

	@Autowired
	JdbcTemplate tpl;

	private static final class TradeMapper implements RowMapper<Trade> {
		public Trade mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Trade(rs.getInt("id"), rs.getString("name"), rs.getDouble("price"), rs.getInt("volume"));
		}
	}

	@Override
	public int createTrade(Trade trade) {
		
		System.out.println("*****************************");

		int rowsAffected = tpl.update("insert into trade (id, stock, price, volume) values (?, ?, ?, ?)", trade.getId(),
				trade.getStockName(), trade.getPrice(), trade.getVolume());

		if (rowsAffected == 0)
			throw (new TradeNotSqlCompatible("Trade was not inserted into database"));
		else
			return trade.getId();
		
	}

	@Override
	public Trade findTradeById(int id) {
		List<Trade> trades = tpl.query("select id, stock, price, volume from trade where id =?", new Object[] { id },
				new TradeMapper());
		if (trades.size() != 1)
			throw (new TradeNotFoundException("Trade  id: " + id + "  was not found."));
		else
			return trades.get(0);
	}

	@Override
	public List findAllTrades() {
		List<Trade> trades = tpl.query("select id, name, price, volume from trade", new TradeMapper());
		if (trades.size() == 0)
			throw (new TradeNotFoundException("No trades were found."));
		else
			return trades;
	}

	@Override
	public int deleteTrade(int id) {

		int rowsAffected = tpl.update("delete from trade where id=?", id );
		if (rowsAffected == 0)
			throw (new TradeNotSqlCompatible("Trade was not deleted from database"));
		else
			return id;
	}

}

package com.citi.training.friday.trade.dao.mysql;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.friday.trade.model.Trade;


@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class MysqlTradeDaoTest {

	@Autowired
	MysqlTradeDao mysqlTradeDao;
	Trade trade = new Trade(123,"NAME",9.99,100);
	

	@Test
	public void createTrade_test() {
		int rowsAffected = mysqlTradeDao.createTrade(trade);
		assertEquals(rowsAffected, 1);
	}

	
	public void findTradeById() {
		Trade returnedtrade = mysqlTradeDao.findTradeById(123);
		assertEquals(returnedtrade.getId(), 123);
	}

	public void findAllTrades() {

	}

	public void deleteTrade(int id) {

	}

}

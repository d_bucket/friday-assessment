package com.citi.training.friday.trade.exception;

public class TradeNotFoundException extends RuntimeException{

	public TradeNotFoundException(String msg) {
		super(msg);
	}
}

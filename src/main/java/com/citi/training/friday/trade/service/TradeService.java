package com.citi.training.friday.trade.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.citi.training.friday.trade.dao.TradeDao;
import com.citi.training.friday.trade.model.Trade;

public class TradeService {

	@Autowired
	TradeDao tradeDao;
	
	int createTrade(Trade trade) {
		return tradeDao.createTrade(trade);
	}

	Trade findTradeById(int id) {
		return tradeDao.findTradeById(id);
	}

	List findAllTrades() {
		return tradeDao.findAllTrades();
	}

	int deleteTrade(int id) {
		return tradeDao.deleteTrade(id);
	}

}

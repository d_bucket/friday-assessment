package com.citi.training.friday.trade.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.citi.training.friday.trade.model.Trade;

@Component
public interface TradeDao {

	int createTrade(Trade trade);

	Trade findTradeById(int id);

	List findAllTrades();

	int deleteTrade(int id);

}

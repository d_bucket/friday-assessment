package com.citi.training.friday.trade.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.citi.training.friday.trade.exception.IncorrectTradeFormatException;
import com.citi.training.friday.trade.model.Trade;

public class TradeTest {

	// initial parameters
	int id = 1;
	String name = "ABC";
	double price = 105.99;
	int volume = 2500000;

	@Test
	public void TradeConstructorTest() {

		Trade trade = new Trade(id, name, price, volume);
		assertNotNull(trade);
	}

	@Test
	public void getId() {
		int otherID = 2;
		Trade trade = new Trade(otherID, name, price, volume);
		assertEquals(trade.getId(), otherID);
	}

	@Test
	public void setId() {
		int otherID = 3;
		Trade trade = new Trade(id, name, price, volume);
		trade.setId(otherID);
		assertEquals(trade.getId(), otherID);
	}

	@Test
	public void getName() {
		Trade trade = new Trade(id, name, price, volume);
		assertEquals(trade.getStockName(), name);
	}

	@Test
	public void setName() {
		String otherName = "QWW";
		Trade trade = new Trade(id, otherName, price, volume);
		trade.setName(otherName);
		assertEquals(trade.getStockName(), otherName);
	}

	@Test
	public void getPrice() {
		Trade trade = new Trade(id, name, price, volume);
		assert(trade.getPrice()==price);
	}

	@Test
	public void setPrice() {
		double otherPrice = 3;
		Trade trade = new Trade(id, name, price, volume);
		trade.setPrice(otherPrice);
		assert(trade.getPrice()== otherPrice);
	}

	@Test
	public void getVolume() {
		Trade trade = new Trade(id, name, price, volume);
		assert(trade.getVolume()==volume);
	}

	@Test
	public void setVolume() {
		int otherVolume = 3;
		Trade trade = new Trade(id, name, price, volume);
		trade.setVolume(otherVolume);
		assert(trade.getVolume()== otherVolume);
	}
}

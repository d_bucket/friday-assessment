package com.citi.training.friday.trade.model;

import com.citi.training.friday.trade.exception.IncorrectTradeFormatException;

public class Trade {

	private int id;
	private String stockName;
	private double price;
	private int volume;

	public Trade() {
		super();
	}

	public Trade(int id, String stockName, double price, int volume) {
		super();
		this.id = id;
		this.setName(stockName);
		this.setPrice(price);
		this.volume = volume;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStockName() {
		return stockName;
	}

	public void setName(String stockName) {
		if (isValidName(stockName))
			this.stockName = stockName;
		else
			throw (new IncorrectTradeFormatException("Stock name format is incorrect"));
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		if (isValidPrice(price))
			this.price = price;
		else
			throw (new IncorrectTradeFormatException("Price format is incorrect"));
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	/**
	 * @param name
	 * @return boolean indicating if valid name format for a stock
	 */
	public boolean isValidName(String stockName) {
		if (stockName == null)
			return false;

		String capitalizedName = stockName.toUpperCase();
		if (capitalizedName.equals(stockName))
			return true;
		else
			return false;
	}

	/**
	 * @param price
	 * @return boolean indicating if valid price format for a stock
	 */
	public boolean isValidPrice(double price) {
		if (price > 0)
			return true;
		else
			return false;
	}

}

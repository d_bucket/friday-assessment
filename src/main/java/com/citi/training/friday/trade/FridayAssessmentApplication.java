package com.citi.training.friday.trade;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FridayAssessmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(FridayAssessmentApplication.class, args);
	}

}

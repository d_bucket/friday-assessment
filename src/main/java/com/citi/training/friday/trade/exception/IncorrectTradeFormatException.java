package com.citi.training.friday.trade.exception;

public class IncorrectTradeFormatException extends RuntimeException {

	public IncorrectTradeFormatException(String msg) {
		super(msg);
	}

}
